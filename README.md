# C_Pico6485

C library to control a Keithley Pico Ammeter 6485 (and similar models). Test program included.

More info here: https://www.gdargaud.net/Hack/KeithleyPico.html
