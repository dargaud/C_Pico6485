pico	= Pico
pico_src= CcobPicoSerial.c

CC=gcc
COPT = -ggdb -O0 -Wall -std=gnu11 -fdiagnostics-color=always -DUSE_COLOR_MSG -DREMOTE=${REMOTE}
TP=-DSTANDALONE_TEST_PROG

$(pico): $(pico_src)
	@#$(CC) $(COPT) -c $<			# Compile as obj to test lib capabilities (optional)
	$(CC) $(COPT)    $^ -o $@ $(TP)	# Compile as standalone test program

clean:
	rm -f Pico core* *.o
