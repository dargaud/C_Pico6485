// From /usr/include/asm-generic/termbits.h
#ifndef __TERMIOS_DEBUG_H
#define __TERMIOS_DEBUG_H

#include <termios.h> 	// POSIX terminal control definitions

#if 0
typedef unsigned char   cc_t;
typedef unsigned int    speed_t;
typedef unsigned int    tcflag_t;

#define NCCS 19
struct termios {
        tcflag_t c_iflag;               /* input mode flags */
        tcflag_t c_oflag;               /* output mode flags */
        tcflag_t c_cflag;               /* control mode flags */
        tcflag_t c_lflag;               /* local mode flags */
        cc_t c_line;                    /* line discipline */
        cc_t c_cc[NCCS];                /* control characters */
};

struct termios2 {
        tcflag_t c_iflag;               /* input mode flags */
        tcflag_t c_oflag;               /* output mode flags */
        tcflag_t c_cflag;               /* control mode flags */
        tcflag_t c_lflag;               /* local mode flags */
        cc_t c_line;                    /* line discipline */
        cc_t c_cc[NCCS];                /* control characters */
        speed_t c_ispeed;               /* input speed */
        speed_t c_ospeed;               /* output speed */
};

struct ktermios {
        tcflag_t c_iflag;               /* input mode flags */
        tcflag_t c_oflag;               /* output mode flags */
        tcflag_t c_cflag;               /* control mode flags */
        tcflag_t c_lflag;               /* local mode flags */
        cc_t c_line;                    /* line discipline */
        cc_t c_cc[NCCS];                /* control characters */
        speed_t c_ispeed;               /* input speed */
        speed_t c_ospeed;               /* output speed */
};
#endif

#define TermiosDebug(options) TermiosDebug_Wrapper( (char[1024]) {0}, options)

static char* TermiosDebug_Wrapper(char* Str, struct termios *options) {
//	static char Str[1024]="";

	/* c_cc characters */
	#define ADD_CC(bit) if (options->c_cc[V##bit]) sprintf(Str+strlen(Str), #bit "=%d ", options->c_cc[V##bit])
	sprintf(Str+strlen(Str), "\nc_cc: ");
	ADD_CC(INTR);
	ADD_CC(QUIT);
	ADD_CC(ERASE);
	ADD_CC(KILL);
	ADD_CC(EOF);
	ADD_CC(TIME);
	ADD_CC(MIN);
	ADD_CC(SWTC);
	ADD_CC(START);
	ADD_CC(STOP);
	ADD_CC(SUSP);
	ADD_CC(EOL);
	ADD_CC(REPRINT);
	ADD_CC(DISCARD);
	ADD_CC(WERASE);
	ADD_CC(LNEXT);
	ADD_CC(EOL2);

	/* c_iflag bits */
	#define ADD_IFLAG(bit) if (options->c_iflag & (bit)) sprintf(Str+strlen(Str), #bit " ")
	sprintf(Str+strlen(Str), "\nc_iflag: ");
	ADD_IFLAG(IGNBRK);
	ADD_IFLAG(BRKINT);
	ADD_IFLAG(IGNPAR);
	ADD_IFLAG(PARMRK);
	ADD_IFLAG(INPCK);
	ADD_IFLAG(ISTRIP);
	ADD_IFLAG(INLCR);
	ADD_IFLAG(IGNCR);
	ADD_IFLAG(ICRNL);
	ADD_IFLAG(IUCLC);
	ADD_IFLAG(IXON);
	ADD_IFLAG(IXANY);
	ADD_IFLAG(IXOFF);
	ADD_IFLAG(IMAXBEL);
	ADD_IFLAG(IUTF8);

	/* c_oflag bits */
	#define ADD_OFLAG(bit) if (options->c_oflag & (bit)) sprintf(Str+strlen(Str), #bit " ")
	#define TEST_OFLAG(bits,val) if ((options->c_oflag & (bits)) ==(val)) sprintf(Str+strlen(Str), #bits "=" #val " ")
	sprintf(Str+strlen(Str), "\nc_oflag: ");
	ADD_OFLAG(OPOST);
	ADD_OFLAG(OLCUC);
	ADD_OFLAG(ONLCR);
	ADD_OFLAG(OCRNL);
	ADD_OFLAG(ONOCR);
	ADD_OFLAG(ONLRET);
	ADD_OFLAG(OFILL);
	ADD_OFLAG(OFDEL);
	TEST_OFLAG(NLDLY,NL0);
	TEST_OFLAG(NLDLY,NL1);
	TEST_OFLAG(CRDLY,CR0);
	TEST_OFLAG(CRDLY,CR1);
	TEST_OFLAG(CRDLY,CR2);
	TEST_OFLAG(CRDLY,CR3);
	TEST_OFLAG(TABDLY,TAB0);
	TEST_OFLAG(TABDLY,TAB1);
	TEST_OFLAG(TABDLY,TAB2);
	TEST_OFLAG(TABDLY,TAB3);
	// #define   XTABS 0014000
	TEST_OFLAG(BSDLY,BS0);
	TEST_OFLAG(BSDLY,BS1);
	TEST_OFLAG(VTDLY,VT0);
	TEST_OFLAG(VTDLY,VT1);
	TEST_OFLAG(FFDLY,FF0);
	TEST_OFLAG(FFDLY,FF1);

	/* c_cflag bit meaning */
	#define ADD_CFLAG(bit) if (options->c_cflag & (bit)) sprintf(Str+strlen(Str), #bit " ")
	#define TEST_CFLAG(bits,val) if ((options->c_cflag & (bits)) ==(val)) sprintf(Str+strlen(Str), #bits "=" #val " ")
	sprintf(Str+strlen(Str), "\nc_cflag: ");
	TEST_CFLAG(CBAUD, B0);
	TEST_CFLAG(CBAUD, B50);
	TEST_CFLAG(CBAUD, B75);
	TEST_CFLAG(CBAUD, B110);
	TEST_CFLAG(CBAUD, B134);
	TEST_CFLAG(CBAUD, B150);
	TEST_CFLAG(CBAUD, B200);
	TEST_CFLAG(CBAUD, B300);
	TEST_CFLAG(CBAUD, B600);
	TEST_CFLAG(CBAUD, B1200);
	TEST_CFLAG(CBAUD, B1800);
	TEST_CFLAG(CBAUD, B2400);
	TEST_CFLAG(CBAUD, B4800);
	TEST_CFLAG(CBAUD, B9600);
	TEST_CFLAG(CBAUD, B19200);
	TEST_CFLAG(CBAUD, B38400);
	//#define EXTA B19200
	//#define EXTB B38400
	TEST_CFLAG(CSIZE, CS5);
	TEST_CFLAG(CSIZE, CS6);
	TEST_CFLAG(CSIZE, CS7);
	TEST_CFLAG(CSIZE, CS8);
	ADD_CFLAG(CSTOPB);
	ADD_CFLAG(CREAD);
	ADD_CFLAG(PARENB);
	ADD_CFLAG(PARODD);
	ADD_CFLAG(HUPCL);
	ADD_CFLAG(CLOCAL);
	//TEST_CFLAG(CBAUDEX, BOTHER);
	TEST_CFLAG(CBAUDEX, B57600);
	TEST_CFLAG(CBAUDEX, B115200);
	TEST_CFLAG(CBAUDEX, B230400);
	TEST_CFLAG(CBAUDEX, B460800);
	TEST_CFLAG(CBAUDEX, B500000);
	TEST_CFLAG(CBAUDEX, B576000);
	TEST_CFLAG(CBAUDEX, B921600);
	TEST_CFLAG(CBAUDEX, B1000000);
	TEST_CFLAG(CBAUDEX, B1152000);
	TEST_CFLAG(CBAUDEX, B1500000);
	TEST_CFLAG(CBAUDEX, B2000000);
	TEST_CFLAG(CBAUDEX, B2500000);
	TEST_CFLAG(CBAUDEX, B3000000);
	TEST_CFLAG(CBAUDEX, B3500000);
	TEST_CFLAG(CBAUDEX, B4000000);
	//#define CIBAUD    002003600000  /* input baud rate */
	ADD_CFLAG(CMSPAR);
	ADD_CFLAG(CRTSCTS);
	//#define IBSHIFT   16            /* Shift from CBAUD to CIBAUD */

	/* c_lflag bits */
	#define ADD_LFLAG(bit) if (options->c_lflag & (bit)) sprintf(Str+strlen(Str), #bit " ")
	sprintf(Str+strlen(Str), "\nc_lflag: ");
	ADD_LFLAG(ISIG);
	ADD_LFLAG(ICANON);
	ADD_LFLAG(XCASE);
	ADD_LFLAG(ECHO);
	ADD_LFLAG(ECHOE);
	ADD_LFLAG(ECHOK);
	ADD_LFLAG(ECHONL);
	ADD_LFLAG(NOFLSH);
	ADD_LFLAG(TOSTOP);
	ADD_LFLAG(ECHOCTL);
	ADD_LFLAG(ECHOPRT);
	ADD_LFLAG(ECHOKE);
	ADD_LFLAG(FLUSHO);
	ADD_LFLAG(PENDIN);
	ADD_LFLAG(IEXTEN);
	//ADD_LFLAG(EXTPROC);	// Not always present

	return Str;
}	
	
#if 0
	/* tcflow() and TCXONC use these */
#define TCOOFF          0
#define TCOON           1
#define TCIOFF          2
#define TCION           3

/* tcflush() and TCFLSH use these */
#define TCIFLUSH        0
#define TCOFLUSH        1
#define TCIOFLUSH       2

/* tcsetattr uses these */
#define TCSANOW         0
#define TCSADRAIN       1
#define TCSAFLUSH       2
#endif
	
#endif	// __TERMIOS_DEBUG_H
