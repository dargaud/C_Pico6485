#ifndef _CCOB_PICO_SERIAL_H
#define _CCOB_PICO_SERIAL_H

typedef struct sPico {
	int msWait;		// Delay between serial operations, in ms, for msleep(). 25ms should be enough for one 256-byte buffer at 9600
	int cHZ;		// PLC integration rate main current frequency (50Hz or 60Hz). Should be set before calling PicoInit
} tPico;
extern tPico Pico;

extern int  PicoInit(char *DevicePath, int ShortInit);
extern void PicoStop(void);

extern int PicoRead(double *Current, double *TimeStamp, int *Status, int Fetch);
extern int PicoTrigger(void);
extern int PicoOperationComplete(void);

extern int PicoSetRange(int R);
extern char* PicoGetRange(void);
extern int PicoSetRate(double PLC);

extern char* PicoStatus(void);
extern char* PicoLastErrMsg(void);

#endif
