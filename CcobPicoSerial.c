// To select the interface on the Keithley, press COMM, select RS 232, then press ENTER
// To configure the RS-232 interface, press CONFIG then COMM when the RS-232 interface is selected, 
// then set baud rate (9600), data bits (8), parity (none), terminator (LF=\n), and flow control (none)

#include <ctype.h>		// iscntrl
#include <errno.h>
#include <fcntl.h>
#include <iso646.h>
#include <stdarg.h>		// va_args
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>		// For usleep()

#include "TermiosDebug.h"

//#include "SerialOpen.h"
#include "CcobPicoSerial.h"
#include "UseColors.h"

tPico Pico={
	.msWait=/*500*/25,	// Delay between serial operations, in ms, for msleep(). 25ms should be enough for one 256-byte buffer at 9600
	.cHZ=50				// PLC integration rate main current frequency (50Hz or 60Hz)
}; 

#undef DD
#define DD (Pico.msWait*1000)	// in us
#define msleep(ms) usleep((ms)*1000)


static char *Tabs="\t";	// So that the various instruments have different alignments to make their logging more readable when intermixed

static int fd=0;		// Negative if failed, 0 if uninit or closed
//static int msPause=0;	// Set this to n to wait some milliseconds between write and read in a RW sequence. 
						// Will be reset for next command
static double LastRate=5;	// In unit of 20ms, 0.01 to cHZ, so 200us to 1s

static char LastErrMsg[1024]="";

///////////////////////////////////////////////////////////////////////////////
/// HIPAR	Path / Such as "/dev/ttyUSB0"
/// HIPAR	Mult / Speed: 1 for 9600, 2 for 19200
/// HIRET	fd / file descriptor to the opened serial port. <0 if error (see errno)
///////////////////////////////////////////////////////////////////////////////
static int StartSerial(char *Path, int Mult) {
	int fd;
	errno=0;
	
	if (0 >= (fd = open(Path, 
		// microcom uses only O_RDWR
		O_RDWR | O_NOCTTY | O_NDELAY )) ) {
		fprintf(stderr, BLD RED "Opening device %s failed: r=%d, errno=%d, %s\n" NRM, 
				Path, fd, errno, strerror(errno));
		return fd;
		}
		
		// See http://www.easysw.com/~mike/serial/serial.html
		
		// If no characters are available for read, 
		// the call to read() will block (wait) until characters come in, 
		// an interval timer expires, or an error occurs.
		// 0 is the normal (blocking) behavior
		if (0>fcntl(fd, F_SETFL, 0 /* FNDELAY */ )) { perror(__func__); return errno; }
		
		struct termios options;
		// Get the current options for the port...
		if (0>tcgetattr(fd, &options)) { perror(__func__); return errno; }
		
		fprintf(stderr, "\nFound in: cflag=0x%x, lflag=0x%x, iflag=0x%x, oflag=0x%x\n", 
				options.c_cflag, options.c_lflag, 
		  options.c_iflag, options.c_oflag /*, options.c_cc*/);
		
		// Set 9600 bps speed by default
		if (0>cfsetospeed(&options, Mult==2 ? B19200 : B9600)) { perror(__func__); return errno; }
		if (0>cfsetispeed(&options, Mult==2 ? B19200 : B9600)) { perror(__func__); return errno; }
		
		// Enable the receiver and set local mode...
		//options.c_cflag |= (CLOCAL | CREAD );
		
		//options.c_lflag |= ICANON | ISIG;			// 
		//options.c_lflag &= ~(ECHO | ECHOE | ICANON | ISIG);	// Clear echo bits
		// Same as microcom:
		options.c_lflag &= ~ICANON; 
		options.c_lflag &= ~(ECHO | ECHOCTL | ECHONL);
		options.c_cflag |= HUPCL /*| CLOCAL | CREAD*/;
		
		//options.c_oflag     &= ~OPOST;
		// Same as microcom:
		options.c_oflag |= ONLCR;
		options.c_iflag &= ~ICRNL;
		
		// See http://unixwiz.net/techtips/termios-vmin-vtime.html
		// This is a pure timed read. If data are available in the input queue, 
		// it's transferred to the caller's buffer up to a maximum of nbytes, 
		// and returned immediately to the caller. 
		// Otherwise the driver blocks until data arrives, 
		// or when VTIME tenths expire from the start of the call. 
		// If the timer expires without data, zero is returned. 
		// A single byte is sufficient to satisfy this read call, 
		// but if more is available in the input queue, it's returned to the caller. 
		// Note that this is an overall timer, not an intercharacter one.
		options.c_cc[VMIN]  = 0/*0*/;
		options.c_cc[VTIME] = 50/*1*/;	// in multiple of 100ms timeout
			// NOTE: "MEAS?" with a PLC of 50 normally takes 1s, but there's overhead, and autozero will triple the time so it can take about 3s
			//       also autorange can triple the duration, so it can take up to 3x3~=10s
			//       also if you use filter or averaging it will increase VTIME or the read() will report NO_REPLY
		
		// [Same as microcom:]
		// This is a counted read that is satisfied only when at least VMIN characters 
		// have been transferred to the caller's buffer - 
		// there is no timing component involved. 
		// This read can be satisfied from the driver's input queue 
		// (where the call could return immediately), 
		// or by waiting for new data to arrive: 
		// in this respect the call could block indefinitely. 
		// We believe that it's undefined behavior if nbytes is less then VMIN. 
		// options.c_cc[VMIN]  = 1;
		// options.c_cc[VTIME] = 0;	// in multiple of 100ms timeout
		
		// set hardware flow control by default - NOT
		//	options.c_cflag |= CRTSCTS;
		options.c_iflag &= ~(IXON | IXOFF | IXANY);
		
		// No parity (8N1)
		options.c_cflag &= ~(PARENB | CSTOPB | CSIZE | CRTSCTS);
		options.c_cflag |= CS8;
		
		
		// Set the new options for the port...
		if (0>tcsetattr(fd, TCSANOW, &options)) { perror(__func__); return errno; }
		
		fprintf(stderr, "Set to  : cflag=0x%x, lflag=0x%x, iflag=0x%x, oflag=0x%x"
		"%s\n", 
		options.c_cflag, options.c_lflag, 
		options.c_iflag, options.c_oflag /*, options.c_cc*/,
		TermiosDebug(&options));
		
		return fd;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Write message on device
/// HIPAR	fd / File descriptor of opened device
/// HIPAR	Msg / String to write
/// HIRET 	Num of chars sent. Check errno if <0
///////////////////////////////////////////////////////////////////////////////
static int Send(int fd, const char Msg[]) {
	int R;
	errno=0;
	if (fd<=0) return fd;
	R=write(fd, Msg, strlen(Msg));
	if      (R<0)  fprintf(stderr, BLD RED "Write failed: r=%d, errno=%d, %s\n" NRM, fd, errno, strerror(errno));
	else if (R==0) fprintf(stderr, BLD RED "Write failed: 0 bytes written\n" NRM);
	return R;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Read message from device
/// HIPAR	fd / File descriptor of opened device
/// HIPAR	Msg / String to read
/// HIPAR	Max / Max number of chars to read in
///////////////////////////////////////////////////////////////////////////////
static int Receive(int fd, char *Msg, int Max) {
	int R;
	errno=0;
	if (fd<=0) { *Msg='\0'; return fd; }
	// TODO: check buffer size
	R=read(fd, Msg, Max);
	if (R<0) fprintf(stderr, BLD RED "Read failed: r=%d, errno=%d, %s\n" NRM, fd, errno, strerror(errno));
	else {
		//		if (R==0) printf("NR\n");	// No Reply
		Msg[R]='\0';
	}
	return R;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Send arbitrary string to Pico
/// HIPAR	Command / As input, string to test, will add \n if necessary
/// HIPAR	Reply / Pass NULL if you don't want a reply. NOTE: queries that want a reply should end with '?'
/// HIRET	Number of character read. Check errno if <0
///////////////////////////////////////////////////////////////////////////////
static int PicoRW(char* Reply, const char* Command, ... ) {
	char Cmd[256], RR[256]="";
	int R;	//, msPCopy=0;
	va_list str_args;

	va_start( str_args, Command );
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored "-Wformat"
	/*int Nb=*/ vsnprintf(Cmd, 256, Command, str_args);
//#pragma clang diagnostic pop
	va_end( str_args );

	printf("Sent :%s%s\n", Tabs, Cmd);
	if (Cmd[strlen(Cmd)-1]!='\n') strcat(Cmd, "\n");
	if (0>=(R=Send(fd, Cmd))) { sprintf(LastErrMsg, "Send failed: %s", strerror(errno)); return R; }
	
#if 0	// if VTIME=150 (ie 15s), then disable this section for a blocked read (with timeout)
// Necessary for *RST, self test and a few others, including MEAS?
	if ((strcasecmp(Command, "MEAS?")==0 or 
	     strcasecmp(Command, "*IDN?")==0 or
	     strcasecmp(Command, "RANGE?")==0 or
	    ) and msPause==0) 
		msPause=/*LastRate**/1000;	// The time we need to wait is proportional to the rate. *200 is not enough
//		msPause=1000.*LastRate/50+10;	// If 1 PLC is 20ms=1/50s, this should be it. Doesn't work though
	if (msPause) msleep(msPCopy=msPause); 
	msPause=0;
	
#endif

	if (Reply==NULL) return R;
	
	if (fd>0) usleep(DD);	// This is only to account for the serial line delay
							// Note that using DD is not enough: Receive returns as soon as data starts incoming on the input queue
							// if the reply is long, we need to wait AFTER Receive() to read the rest if the reply is incomplete

	if (0<(R=Receive(fd, RR, 255))) {
		char *P;
		if (NULL==(P=strchr(RR, '\n')) and NULL==(P=strchr(RR, '\r')) ) { 
			/*printf(BLD RED "%sIncomplete read (%s) - trying again\n" NRM, Tabs, RR);*/ usleep(DD); R+=Receive(fd, &RR[R], 255-R); }
		if (NULL==(P=strchr(RR, '\n')) and NULL==(P=strchr(RR, '\r')) ) { 
			/*printf(BLD RED "%sIncomplete read (%s) - trying again\n" NRM, Tabs, RR);*/ usleep(DD); R+=Receive(fd, &RR[R], 255-R); }
		if (NULL==(P=strchr(RR, '\n')) and NULL==(P=strchr(RR, '\r')) ) { 
			printf(BLD RED "%sIncomplete read (%s) - trying a 3rd time and then giving up\n" NRM, Tabs, RR); usleep(DD); R+=Receive(fd, &RR[R], 255-R); }
				
		while (RR[0]!='\0' and iscntrl(RR[strlen(RR)-1])) RR[strlen(RR)-1]='\0';
		printf("Reply:%s%s\n", Tabs, RR);
		if (Reply) strcpy(Reply, RR);
		if (fd>0) usleep(DD);
	} else if (R<0) { sprintf(LastErrMsg, "Received failed: %s", strerror(errno)); return R; }
	else  printf(BLD RED "%sNo reply for: %s" NRM, Tabs, Cmd);
	
	// Debugging misformed commands. Use it if there are error messages you can't trace to past commands
/*	if (0!=strcmp(Command, "SYSTem:ERRor:ALL?")) {
		char Other[256]="";
		PicoRW(Other, "SYSTem:ERRor:ALL?");
		if (0!=strcmp(Other, "0,\"No error\"")) printf(BLD RED "%s############################### (Pause was %dms)\n" NRM, Tabs, msPCopy);
	}
*/
	return R;
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Return status register string.
/// HIPAR	Status/As returned by *STB? normally 0 to 0xFF. See section "10.8 Status Structure" of 6585-Instruction Manual
/// HIRET	Message
////////////////////////////////////////////////////////////////////////////////
static char* GetStatusRegisterMsg(int Status) {
	static char List[1024]; List[0]='\0';
	if (Status==0) return "N/A";
	if ((Status>>0)&1)                                    strcat(List, "Measurement status (MSB)");
	if ((Status>>1)&1) { if (List[0]) strcat(List, ", "); strcat(List, "N/A"); }
	if ((Status>>2)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Error available (EAV)"); } // Also read error queue
	if ((Status>>3)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Questionable summary bit (QSB)"); }
	if ((Status>>4)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Message available (MAV)"); } // Also read message queue
	if ((Status>>5)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Event summary bit (ESB)"); }
	if ((Status>>6)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Request service (RQS)/master summary status (MSS)"); }
	if ((Status>>7)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Operation summary (OSB)"); }
	return List[0] ? List : "Unknown status value";
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Return standard event status string.
/// HIPAR	Status/As returned by *ESR?. See section "10.11 Status Structure" of 6585-Instruction Manual
/// HIRET	Message
////////////////////////////////////////////////////////////////////////////////
static char* GetStandardEventStatusMsg(int Status) {
	static char List[1024]; List[0]='\0';
	if (Status==0) return "N/A";
	if ((Status>>0)&1)                                    strcat(List, "Operation complete");
	if ((Status>>1)&1) { if (List[0]) strcat(List, ", "); strcat(List, "N/A"); }
	if ((Status>>2)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Query error (QYE)"); }
	if ((Status>>3)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Device-dependent error (DDE)"); }
	if ((Status>>4)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Execution error (EXE)"); }
	if ((Status>>5)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Command error (CME)"); }
	if ((Status>>6)&1) { if (List[0]) strcat(List, ", "); strcat(List, "User request (URQ)"); }
	if ((Status>>7)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Power ON (PON)"); }
	return List[0] ? List : "Unknown status value";
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Return operation event status string.
/// HIPAR	Status/As returned by STATus:OPERation:CONDition?. See section "10.12 Status Structure" of 6585-Instruction Manual
/// HIRET	Message
////////////////////////////////////////////////////////////////////////////////
static char* GetOperationEventStatusMsg(int Status) {
	static char List[1024]=""; List[0]='\0';
	if (Status==0) return "N/A";
	if ((Status>> 0)&1)                                    strcat(List, "Calibrating");
	if ((Status>> 5)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Waiting for trigger event (Trig)"); }
	if ((Status>> 6)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Waiting for arm event (Arm)"); }
	if ((Status>>10)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Idle state (Idle)"); }
	return List[0] ? List : "Unknown status value";
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Return measurement event status string.
/// HIPAR	Status/As returned by STATus:MEASurement:CONDition?. See section "10.13 Status Structure" of 6585-Instruction Manual
/// HIRET	Message
////////////////////////////////////////////////////////////////////////////////
static char* GetMeasurementEventStatusMsg(int Status) {
	static char List[1024]=""; List[0]='\0';
	if (Status==0) return "N/A";
	if ((Status>> 0)&1)                                    strcat(List, "N/A");
	if ((Status>> 1)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Low limit 1 fail (LL1F)"); }
	if ((Status>> 2)&1) { if (List[0]) strcat(List, ", "); strcat(List, "High limit 1 fail (HL1F)"); }
	if ((Status>> 3)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Low limit 2 fail (LL2F)"); }
	if ((Status>> 4)&1) { if (List[0]) strcat(List, ", "); strcat(List, "High limit 2 fail (HL2F)"); }
	if ((Status>> 5)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Limits pass (LP)"); }
	if ((Status>> 6)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Reading available (RAV)"); }
	if ((Status>> 7)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Reading overflow (ROF)"); }
	if ((Status>> 8)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Buffer available (BAV)"); }
	if ((Status>> 9)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Buffer full (BFL)"); }
	if ((Status>>10)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Input overvoltage (IOV)"); }
	return List[0] ? List : "Unknown status value";
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Return questionable event status string.
/// HIPAR	Status/As returned by STATus:QUEStionable:CONDition?. See section "10.15 Status Structure" of 6585-Instruction Manual
/// HIRET	Message
////////////////////////////////////////////////////////////////////////////////
static char* GetQuestionableEventStatusMsg(int Status) {
	static char List[1024]=""; List[0]='\0';
	if (Status==0) return "N/A";
	if ((Status>> 7)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Calibration summary (Cal)"); }
	if ((Status>>14)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Command warning (Warn)"); }
	return List[0] ? List : "Unknown status value";
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Return element status string.
/// HIPAR	Status/As returned by MEAS?. See section "13.6 DISPlay,FORMat,SYSTem" of 6585-Instruction Manual
/// HIRET	Message(s)
////////////////////////////////////////////////////////////////////////////////
static char* GetElementStatusMsg(int Status) {
	static char List[1024]=""; List[0]='\0';
	if (Status==0) return "N/A";
	if ((Status>> 0)&1)                                    strcat(List, "Over-range (overflowed reading)");
	if ((Status>> 1)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Averaging filter enabled"); }
	if ((Status>> 2)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Measurement performed with CALC1 enabled"); }
	if ((Status>> 3)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Null for CALC2 is enabled"); }
	if ((Status>> 4)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Limit test (CALC2) is enabled"); }
	if ((Status>> 5)&1) { if (List[0]) strcat(List, ", "); strcat(List, "CALC2:LIM1 test failed"); }
	if ((Status>> 6)&1) { if (List[0]) strcat(List, ", "); strcat(List, "CALC2:LIM2 test failed"); }
	if ((Status>> 7)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Overvoltage condition on the input"); }
	if ((Status>> 8)&1) { if (List[0]) strcat(List, ", "); strcat(List, "N/A"); }
	if ((Status>> 9)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Zero check is enabled"); }
	if ((Status>>10)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Zero correct is enabled"); }
	return List[0] ? List : "Unknown status value";
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Read all possible status registers
/// HIRET	String with a summary of status strings
///////////////////////////////////////////////////////////////////////////////
static char* GetAllStatuses(void) {
	char Reply[256]="", *P;
	int Cond, Enable, Event, Status, Err, Msg;
	static char Details[1024]; Details[0]='\0';
	
	PicoRW(Reply, "STATus:QUEue:ENABle?");
	if (atoi(Reply)) PicoRW(Reply, "STATus:QUEue?");

	PicoRW(Reply, "*SRE?");		// Service Request Enable register. Should be 0
	do {
		PicoRW(Reply, "*STB?");		printf(BLD YEL "%sStatus register: %s\n" NRM, Tabs, P=GetStatusRegisterMsg(Status=atoi(Reply)));
		if (atoi(Reply)) sprintf(Details+strlen(Details), "%s\n", P);
		if ((Err=(Status>>2)&1)) {
//			msPause=500;
			PicoRW(Reply, "SYSTem:ERRor:ALL?");	printf(BLD RED "%sError queue: %s\n" NRM, Tabs, Reply);
			sprintf(Details+strlen(Details), "%s\n", Reply);
		}
		if ((Msg=(Status>>4)&1)) {
			PicoRW(Reply, "STATUS:QUEUE?");	
			if (Reply[0]=='\0' or 0==strncmp("0,\"No error\"", Reply, 12)) Msg=0;	// Spurious MAV ?
			else {
				printf(BLD YEL "%sMessage queue: %s\n" NRM, Tabs, Reply);
				sprintf(Details+strlen(Details), "%s\n", Reply);
			}
		}
	} while (Err or Msg);

	// Read the standard event enable register and clear it.
	PicoRW(Reply, "*ESE?");		// Standard Event Enable register
	PicoRW(Reply, "*ESR?");	printf(BLD YEL "%sStandard event status: %s\n" NRM, Tabs, P=GetStandardEventStatusMsg(atoi(Reply)));
	if (atoi(Reply)) sprintf(Details+strlen(Details), "%s\n", Reply);

	// Read condition registers
	PicoRW(Reply, "STATus:OPERation:CONDition?");	Cond=atoi(Reply); 
	PicoRW(Reply, "STATus:OPERation:ENABle?");		Enable=atoi(Reply); 
	PicoRW(Reply, "STATus:OPERation:EVENT?");		Event=atoi(Reply);
	if (Event) printf(BLD YEL "%sOperation condition:0x%x, enable:0x%x, event status: %s\n" NRM, 
		   Tabs, Cond, Enable, GetOperationEventStatusMsg(atoi(Reply)));
	
	PicoRW(Reply, "STATus:MEASurement:CONDition?");	Cond=atoi(Reply); 
	PicoRW(Reply, "STATus:MEASurement:ENABle?");	Enable=atoi(Reply); 
	PicoRW(Reply, "STATus:MEASurement:EVENT?");		Event=atoi(Reply);
	if (Event) printf(BLD YEL "%sMeasurement condition:0x%x, enable:0x%x, event status: %s\n" NRM, 
		   Tabs, Cond, Enable, GetMeasurementEventStatusMsg(atoi(Reply)));
	
	PicoRW(Reply, "STATus:QUEStionable:CONDition?");	Cond=atoi(Reply);
	PicoRW(Reply, "STATus:QUEStionable:ENABle?");		Enable=atoi(Reply);
	PicoRW(Reply, "STATus:QUEStionable:EVENT?");		Event=atoi(Reply);
	if (Event) printf(BLD YEL "%sQuestionable condition:0x%x, enable:0x%x, event status: %s\n" NRM, 
		   Tabs, Cond, Enable, GetQuestionableEventStatusMsg(atoi(Reply)));

	return Details;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Init sequence of the Pico
/// HIPAR	DevicePath / Serial device such as /dev/ttyS0
/// HIPAR	ShortInit / If true, only initializes the device, do not send any commands
/// HIRET	0 if no error
///////////////////////////////////////////////////////////////////////////////
int PicoInit(char *DevicePath, int ShortInit) {
	char Reply[256]="";
	errno=0;
	fd=StartSerial(DevicePath, 1);
	if (fd<=0) { sprintf(LastErrMsg, "Pico init error: %s on %s- If you continue to run, the data will be SIMULATED", 
					strerror(errno), DevicePath); 
				 printf(BLD RED "%s%s\n" NRM, Tabs, LastErrMsg); return errno; }
	putchar('\n');
	if (ShortInit) return 0;
	
	PicoRW(Reply, "*IDN?"); 
	if (Reply[0]=='\0') { 	// 2nd attempt, sometimes the 1st read fails after boot
		PicoRW(Reply, "*IDN?"); 
		if (Reply[0]=='\0') { 
			sprintf(LastErrMsg, "Pico is not replying - Aborting"); 
			printf(BLD RED "%s%s\n" NRM, Tabs, LastErrMsg);
			return 1;
	}	}
	PicoRW(Reply, "SYST:VERSION?");
//	msPause=2000; 
	PicoRW(NULL, "*RST");
//	msPause=2000; 
	PicoRW(Reply, "*TST?"); if (atoi(Reply)) { sprintf(LastErrMsg, "Self-test FAILED"); printf(BLD RED "%s%s\n" NRM, Tabs, LastErrMsg); return 100; }
							else                  printf(BLD GRN "%sSelf-test PASSED\n" NRM, Tabs); 
	
	// Clear everything
	PicoRW(NULL, "*CLS");
	PicoRW(NULL, "STATUS:PRESET");		// Clear all presets
	PicoRW(NULL, "STATUS:QUEUE:CLEAR");
	PicoRW(NULL, "SYSTEM:CLEAR");
	
	// Options and statuses
	PicoRW(Reply, "*OPT?");
	GetAllStatuses();
	//	PicoRW(NULL, "DISP:ENAB OFF");

	// Various settings
	PicoSetRange(-1);	// Auto
	PicoSetRate(5);		// Slowest

	// Autozero: To help maintain stability and accuracy over time and changes in temperature, the Model
	// 6485/6487 periodically measures internal voltages corresponding to offsets (zero) and
	//amplifier gains; a process known as autozeroing. With autozero disabled, measurement
	// speed increases up to three times, but measurement accuracy will be reduced.
	// It is recommended that autozero only be disabled for short periods of time.
	PicoRW(NULL, "SYST:AZER ON");
	
	PicoRW(NULL, "SYST:ZCH ON");
	PicoRW(NULL, "INIT");
	PicoRW(NULL, "SYST:ZCOR:ACQ");
	PicoRW(NULL, "SYST:ZCH OFF");
	PicoRW(NULL, "SYST:ZCOR ON");
//	PicoRW(NULL, "CONF:CURR");	// Redundant with READ? but necessary (?) for INIT + FETCH?
	
	PicoRW(NULL, "SYST:TIME:RESET");	// So the first READ? will return close to zero, even if we don't care
	
	// The following will slow measurements down
/*	PicoRW(NULL, "MED <b>");			// Enable (ON) or disable (OFF) median filter.
	PicoRW(NULL, "MED:RANK <n>");		// Specify median filter rank: 1 to 5.
	PicoRW(NULL, "AVER <b>");			// Enable (ON) or disable (OFF) digital filter.
	PicoRW(NULL, "AVER:TCON <name>");	// Select filter control: MOVing or REPeat.
	PicoRW(NULL, "AVER:COUNt <n>");		// Specify filter count: 2 to 100.
*/

//	msPause=1000; PicoRW(NULL, "*WAI");	// Useless ?
//	PicoRW(NULL, "INIT");		// READ? is INIT and FETCh?
//	PicoRW(Reply, "READ?");
        
	// Do a dummy read because the 1st value is often invalid
	double F=0, T=0;
	int S=0;
	PicoRead(&F, &T, &S, 0);
	PicoRead(&F, &T, &S, 0);

//	GetAllStatuses();
	
	printf(BLD GRN "================== PICO READY ==================\n" NRM);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	This call is not necessary for the hardware (no need for a signal catch)
///////////////////////////////////////////////////////////////////////////////
void PicoStop(void) {
//	PicoRW(NULL, "DISP:ENAB ON");
	if (fd<=0) return;
	close(fd); fd=0;
	printf(BLD YEL "================== PICO CLOSED ==================\n" NRM);
}

static char *Range[]   ={"20e-3", "2e-3", "200e-6", "20e-6", "2e-6", "200e-9", "20e-9", "2e-9"};	// in A
static char *RangeAlt[]={"20m",   "2m",   "200u",   "20u",   "2u",   "200n",   "20n",   "2n"};		// in A

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Set the range
/// HIPAR	R / Range index: -1 (AUTO) or 0 to 7 in the Range[] array
/// HIRET	0 if no error
///////////////////////////////////////////////////////////////////////////////
int PicoSetRange(int R) {
	char Reply[256]="";
	if (R<-1 or 7<R) { sprintf(LastErrMsg, "Invalid range"); printf(BLD RED "%s%s\n" NRM, Tabs, LastErrMsg); return 1; }
//	msPause=500; 
	PicoOperationComplete();
	PicoRW(NULL, "SENS:CURR:RANG:AUTO %s", R==-1 ? "ON" : "OFF"); 
	PicoOperationComplete();
	if (R>=0) {
//		msPause=500; 
		PicoRW(NULL, "CONF:CURR");
		PicoOperationComplete();
		PicoRW(NULL, "SENS:CURR:RANG %s", Range[R]);
		PicoOperationComplete();
	}
	
	// Verification
	PicoRW(Reply, "RANG?");	printf(BLD YEL "Pico :%sRange: %s\n" NRM, Tabs, Reply);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Get the range as a string
/// HIRET	0 if no error
///////////////////////////////////////////////////////////////////////////////
char* PicoGetRange(void) {
	char Reply[256]="";
	static char String[1024]; String[0]='\0';
	PicoOperationComplete();
	PicoRW(Reply, "RANG?");
	sprintf(String+strlen(String), "Range: %sA (Possible values: -1:AUTO", Reply);
	for (int i=0; i<=7; i++) {
		double Ratio=atof(Reply)/atof(Range[i]);
		int This=(0.8<Ratio and Ratio<1.2);	// 5% difference between set and get range;
		sprintf(String+strlen(String), ", %s%d:%sA%s", This?"<<<":"", i, RangeAlt[i], This?">>>":"");
	}
	sprintf(String+strlen(String), ")\n");
	return String;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Set the integration rate
/// HIPAR	PLC / in PLC units (=1/50Hz=20ms): 0.01 to cHZ (50 or 60Hz)
/// HIRET	0 if no error
///////////////////////////////////////////////////////////////////////////////
int PicoSetRate(double PLC) {
	char Reply[256]="";
	if (PLC<0.01 or Pico.cHZ<PLC) { sprintf(LastErrMsg, "Invalid PLC %.3f, range is 0.01..%d", PLC, Pico.cHZ); 
									 printf(BLD RED "%s%s\n" NRM, Tabs, LastErrMsg); return 1; }
	//	msPause=500; 
	PicoRW(NULL, "NPLC %.2f", PLC);
	if (PLC==5)  printf(BLD YEL "%sRate: SLOW\n" NRM, Tabs);
	if (PLC==1)  printf(BLD YEL "%sRate: MED\n" NRM, Tabs);
	if (PLC==.1) printf(BLD YEL "%sRate: FAST\n" NRM, Tabs);
	LastRate=PLC;
	
	usleep(1000000*PLC/Pico.cHZ);
	PicoOperationComplete();
	
	// Verification
	PicoRW(Reply, "NPLC?");	LastRate=atof(Reply);
	printf(BLD YEL "Pico :%sIntegration rate: %s (=%.4fs)%s\n" NRM, Tabs, Reply, LastRate/Pico.cHZ, 
		   0==strcmp(Reply, "5.00") ? " (SLOW)" :
		   0==strcmp(Reply, "1.00") ? " (MED)"  :
		   0==strcmp(Reply, "0.10") ? " (FAST)" : ""
	);
	return PLC<0.01 or Pico.cHZ<PLC;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Wait for operation complete
/// HIRET	0 if no error
///////////////////////////////////////////////////////////////////////////////
int PicoOperationComplete(void) {
	char Reply[256]="";
	if (fd<=0) return 0;	// Simulation
	do { PicoRW(Reply, "*OPC?"); } while (atoi(Reply)!=1);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Read and convert values from Pico
/// HIFN	MPTE: there are more elaborate ways to make the measurements, see "3-5 Measurement"
/// OUT		Current, TimeStamp, Status
/// HIPAR	Fetch/Do a FETCH instead of a read (use after trigger)
/// HIRET	0 if no error
///////////////////////////////////////////////////////////////////////////////
int PicoRead(double *Current, double *TimeStamp, int *Status, int Fetch) {
	char Reply[256]="";
	double St;
	int Ret=0;
	if (fd<=0) {	// Simulation
		static time_t Ref=0; if (Ref==0) Ref=time(NULL);
		static double RefCur=0;
		//*Current=1e-3*rand()/RAND_MAX;	// Either random data...
		*Current=(RefCur+=1e-6);			// ...or increasing data
		*TimeStamp=difftime(time(NULL), Ref);
		*Status=0;
		return 0;
	}
	//PicoRW(NULL, "SYST:ZCOR OFF");
	// NOTE: maybe the reason the read sometimes fails is we don't wait enough before reading.
	
	// NOTE: Basically MEASure? and READ? do initiate a new measurement, which may require a long time to complete depending on instrument type.
	// On the other hand, FETCh? just queries measurement data *WITHOUT* initiating new measurement.
	// The measurement data that the FETCh? query returns is the cached data that was acquired at previous MEASure/READ or INITiate (so it's basically useless for regular measurements)
	// (INITiate command initiates a new measurement but does not query the data.)
	// Therefore using FETCh? query without initating will cause a SCPI error.
	
	PicoOperationComplete();
	if (Fetch) {
		Ret=PicoRW(Reply, "FETCH?");
	} else {
		                    Ret=PicoRW(Reply, "MEAS?");
		if (Reply[0]=='\0') Ret=PicoRW(Reply, "MEAS?");	// Try again
		if (Reply[0]=='\0') Ret=PicoRW(Reply, "MEAS?");	// Try again
	}
	if (Reply[0]=='\0') *Current=*TimeStamp=*Status=0;	// Give up !
	else {	// Expected read at this stage is: Current (A), timestamp, status
		int N=sscanf(Reply, "%lfA,%lf,%lf", Current, TimeStamp, &St);
		if (N!=3) printf(BLD RED "%sIncomplete scanf: %d\n" NRM, Tabs, N);
		printf(BLD WHT "%sCurrent:%g, TimeStamp:%f, St:%.0f\n"NRM, Tabs, *Current, *TimeStamp, St);
	}		// St should be an int but is actually written as a float
	*Status=(int)(St+0.5);	// Round to avoid epsilon errors
	if (*Status)        printf(BLD YEL "%sMeasurement status: %s\n" NRM, Tabs, GetElementStatusMsg(*Status));
	if (Reply[0]=='\0') { sprintf(LastErrMsg, "Measurement failed: no reply"); printf(BLD RED "%s%s\n" NRM, Tabs, LastErrMsg); return 1; }
	return Ret<=0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Trigger a reading (INIT). Requires a later FETCH? to read the result
/// HIFN	This function returns immediately (no sync delay)
/// HIRET	0 if no error
///////////////////////////////////////////////////////////////////////////////
int PicoTrigger(void) {
	int Ret=0;
	if (fd<=0) return 0;	// Simulation
/*	     if (0>=(Ret=Send(fd, "CONF:CURR\n"))) sprintf(LastErrMsg, "CONF:CURR failed: %s", strerror(errno));
	else*/	// CONF:CURR seems to put back the autorange, which we want to avoid 
	     if (0>=(Ret=Send(fd, "INIT\n"))     ) sprintf(LastErrMsg, "Trigger failed: %s",   strerror(errno));
	return Ret<=0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Return identifier and lengthy status string
/// HIRET	The status system of the Keithley is quite complex and it's hard to tell if the system is working or not
/// HIRET	My rule of thumb is: if it's replying, then it's working !
///////////////////////////////////////////////////////////////////////////////
char* PicoStatus(void) {
	char Reply[256]="";
	static char String[1024]; String[0]='\0';
	if (fd==0) return "Pico not initialized or closed (returned measurements are simulated)";
	if (fd<0)  return "Pico init error (device not found ?) -> SIMULATION";
	PicoRW(Reply, "*IDN?"); if (Reply[0]=='\0') return "Pico is not replying";
	sprintf(String, "%s\n%s\n%s\n", Reply, GetAllStatuses(), PicoGetRange());

	PicoRW(Reply, "NPLC?");	LastRate=atof(Reply);
	sprintf(String+strlen(String), "Integration rate: %sPLC (=%.1fms)%s", Reply, 1000*LastRate/Pico.cHZ,
		0==strcmp(Reply, "5.00") ? " (SLOW)" :
		0==strcmp(Reply, "1.00") ? " (MED)"  :
		0==strcmp(Reply, "0.10") ? " (FAST)" : ""
	); 
	return String;
}

///////////////////////////////////////////////////////////////////////////////
char* PicoLastErrMsg(void) {
	return LastErrMsg;
}

///////////////////////////////////////////////////////////////////////////////

#ifdef STANDALONE_TEST_PROG
// Compile with: gcc -DSTANDALONE_TEST_PROG CcobPicoSerial.c SerialOpen.c -o TestPico
#include <signal.h>
static FILE *File=NULL;

static void intHandler(int Signal) {
	puts(BLD RED "\n*** Abort - Closing file ***\n" NRM);
	fclose(File); File=NULL;
	exit(2);
}


int main(int argc, char *argv[]) {
	char Reply[256]="", *Device="/dev/ttyUSB0";
	double F=0, T=0;
	int Ret=0, S=0, Shift=1, NoInit=0, ReadDelay=0;
	
	if (argc>1 and (0==strcmp("-h", argv[1]) or 0==strcmp("--help", argv[1]))) {
		fprintf(stderr, BLD WHT "%s [-d=dev] [-noinit] [-r=N] [Cmd1] [Cmd2]...\n"
				"\t-d=dev\tDevice name such as /dev/ttyS0 (default is /dev/ttyUSB0). Run 'dmesg' for connection info\n"
				"\t-noinit\tDo not send any SCPI commands during the init sequence\n"
				"\t-r=N\tExecute a read every N seconds to file PicoRead.csv (endless loop, no Cmd* arguments, autorange, max 1s integration)\n"
				"\tCmd\tList of optional commands to pass to the Keithley (default is 'MEAS?')\n" 
				"If no commands are submitted, it will do some measurements at various ranges and integration rates\n"
				NRM, argv[0]);
		return 1;
	}

	dup2(1, 2);  //redirects stderr to stdout below this line.
	
	for (int i=1; i<argc; i++) {	// Assumed to be first arguments
		if (0==strncmp(argv[i], "-d=",  3)) { Shift++; Device=argv[i]+3;          printf("Using device %s\n", Device); }
		if (0==strcmp (argv[i], "-noinit")) { Shift++; NoInit=1;                  printf("NO INIT\n");}
		if (0==strncmp(argv[i], "-r=",  3)) { Shift++; ReadDelay=atoi(argv[i]+3); printf("Looped read every %ds\n", ReadDelay); }
	}

	if (( Ret=PicoInit(Device, NoInit) )) /*return Ret*/Ret=0;

	if (!NoInit) printf(BLD WHT "%s\n" NRM, PicoStatus());

	if (ReadDelay!=0) {	// Start timed sequence
		if (Shift!=argc) printf(BLD RED "Command line parameters will be ignored in timed loop\n" NRM);
		printf(BLD WHT "Configuring...\n" NRM);
		Ret|=PicoSetRange(-1);	// -1 for Auto or 0 to 7 in the Range[] array
		sleep(1);
		Ret|=PicoSetRate(Pico.cHZ);	// cHZ to 0.01
		sleep(2);
		signal(SIGINT,  intHandler);   // Ctrl-C
		
		time_t CurrentTime; struct tm CurrentTM;
		
		CurrentTime=time (NULL);
		char FileName[80];
		strftime(FileName, 80, "PicoRead_%Y%m%d-%H%M%S.csv", localtime_r (&CurrentTime, &CurrentTM));
		printf(BLD WHT "\nStarting timed loop in %s. Press Ctrl-C to quit.\n\n" NRM, FileName);
		File=fopen(FileName, "a");
		
		fprintf(File, "Date\tnA\n");
		while (1 /*Ret!=0*/) {
			F=0;
			CurrentTime=time (NULL);
			char TimeStr[80];
			strftime(TimeStr, 80, "%Y%m%d-%H%M%S", localtime_r (&CurrentTime, &CurrentTM));
			Ret|=PicoRead(&F, &T, &S, 0);	printf(BLD YEL "%s%.3fnA\n" NRM, Tabs, F*1e9);
			fprintf(File, "%s\t%.3f\n", TimeStr, F*1e9);
			fflush(0);
			sleep(ReadDelay);
		}
		fclose(File); File=0;
	}
	else if (Shift==argc) {	// Default command(s)
		printf("================== Start of default commands ==================\n");
		Ret|=PicoSetRate(5);
		for (int R=-1; R<=7; R++) {
			Ret|=PicoSetRange(R);
			Ret|=PicoRead(&F, &T, &S, 0); 	printf(BLD YEL "%s%.4gA\n" NRM, Tabs, F);
			putchar('\n');
		}
		Ret|=PicoSetRange(-1);
		
		time_t t;
		Ret|=PicoSetRate(Pico.cHZ);  t=time(NULL); Ret|=PicoRead(&F, &T, &S, 0);	printf(BLD YEL "%s%.4gA, read time<%.0fs\n\n" NRM, Tabs, F, 1+difftime(time(NULL), t));
		Ret|=PicoSetRate(20);  t=time(NULL); Ret|=PicoRead(&F, &T, &S, 0);	printf(BLD YEL "%s%.4gA, read time<%.0fs\n\n" NRM, Tabs, F, 1+difftime(time(NULL), t));
		Ret|=PicoSetRate(10);  t=time(NULL); Ret|=PicoRead(&F, &T, &S, 0);	printf(BLD YEL "%s%.4gA, read time<%.0fs\n\n" NRM, Tabs, F, 1+difftime(time(NULL), t));
		Ret|=PicoSetRate(5);   t=time(NULL); Ret|=PicoRead(&F, &T, &S, 0);	printf(BLD YEL "%s%.4gA, read time<%.0fs\n\n" NRM, Tabs, F, 1+difftime(time(NULL), t));
		Ret|=PicoSetRate(2);   t=time(NULL); Ret|=PicoRead(&F, &T, &S, 0);	printf(BLD YEL "%s%.4gA, read time<%.0fs\n\n" NRM, Tabs, F, 1+difftime(time(NULL), t));
		Ret|=PicoSetRate(1);   t=time(NULL); Ret|=PicoRead(&F, &T, &S, 0);	printf(BLD YEL "%s%.4gA, read time<%.0fs\n\n" NRM, Tabs, F, 1+difftime(time(NULL), t));
		Ret|=PicoSetRate(.5);  t=time(NULL); Ret|=PicoRead(&F, &T, &S, 0);	printf(BLD YEL "%s%.4gA, read time<%.0fs\n\n" NRM, Tabs, F, 1+difftime(time(NULL), t));
		Ret|=PicoSetRate(.2);  t=time(NULL); Ret|=PicoRead(&F, &T, &S, 0);	printf(BLD YEL "%s%.4gA, read time<%.0fs\n\n" NRM, Tabs, F, 1+difftime(time(NULL), t));
		Ret|=PicoSetRate(.1);  t=time(NULL); Ret|=PicoRead(&F, &T, &S, 0);	printf(BLD YEL "%s%.4gA, read time<%.0fs\n\n" NRM, Tabs, F, 1+difftime(time(NULL), t));
		Ret|=PicoSetRate(.05); t=time(NULL); Ret|=PicoRead(&F, &T, &S, 0);	printf(BLD YEL "%s%.4gA, read time<%.0fs\n\n" NRM, Tabs, F, 1+difftime(time(NULL), t));
		Ret|=PicoSetRate(.02); t=time(NULL); Ret|=PicoRead(&F, &T, &S, 0);	printf(BLD YEL "%s%.4gA, read time<%.0fs\n\n" NRM, Tabs, F, 1+difftime(time(NULL), t));
		Ret|=PicoSetRate(.01); t=time(NULL); Ret|=PicoRead(&F, &T, &S, 0);	printf(BLD YEL "%s%.4gA, read time<%.0fs\n\n" NRM, Tabs, F, 1+difftime(time(NULL), t));

		Ret|=PicoSetRate(Pico.cHZ);
		Ret|=PicoSetRange(3);
		Ret|=PicoTrigger(); sleep(1);
		t=time(NULL); Ret|=PicoRead(&F, &T, &S, 1);	printf(BLD YEL "%s%.4gA (INIT+FETCH?), read time<%.0fs\n\n" NRM, Tabs, F, 1+difftime(time(NULL), t));

		printf("================== End of default commands ==================\n");
	}
	else {
		printf(BLD GRN "\n================== Start of user commands ==================\n" NRM);
		for (int i=Shift; i<argc; i++) {
//			msPause=1000;
			Ret|=PicoRW(Reply, argv[i]);
			putchar('\n');
		}
		printf(BLD GRN "================== End of user commands ==================\n" NRM);

		if (Ret) printf(BLD RED "Error present\n" NRM);
	}

	if (!NoInit) printf(BLD WHT "%s\n" NRM, PicoStatus());

	PicoStop();
	return Ret;
}
#endif
